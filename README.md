# Service Usage

* KIT bwSync&Share

## Plotting

* Plotting is be performed in the [Plotting project](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci).

## Data + Weighing

./stats/data.csv - Shedule: weekly

| Data | Weighing | Unit |
| ----- | ----- | ----- |
| active bwsyncandsahre users last 24h | 100% | Quantity/Number |
| all bwsyncandshare users | 100% | Quantity/Number |
| all bwsyncandshare files | 100% | Quantity/Number|
| all bwsyncandshare shares | 100% | Quantity/Number |

./stats/active_users_last_1_h-data-2021-07-02_13_49_18.csv - Shedule: every 8 hours

| Data | Weighing | Unit |
| ----- | ----- | ----- |
| active bwsyncandsahre users last 1h | 100% | Qantity/Number |


## Schedule

* weekly 
